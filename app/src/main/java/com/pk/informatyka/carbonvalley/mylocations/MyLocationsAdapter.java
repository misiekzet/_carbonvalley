package com.pk.informatyka.carbonvalley.mylocations;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.pk.informatyka.carbonvalley.R;

import java.util.ArrayList;

public class MyLocationsAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<MyLocation> mDataSource;
    private MyLocationsPreferences preferences;

    public MyLocationsAdapter(Context context, ArrayList<MyLocation> items) {
        this.mContext = context;
        this.mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        preferences =  new MyLocationsPreferences(mContext);
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        final View rowView = mInflater.inflate(R.layout.my_locations_row, parent, false);

        TextView addressTextView = (TextView) rowView.findViewById(R.id.address_text_view);
        TextView caqiTextView = (TextView) rowView.findViewById(R.id.caqi_text_view);

        Button removeButton = (Button) rowView.findViewById(R.id.remove_button);

        final MyLocation myLocation = (MyLocation) getItem(position);

        addressTextView.setText(myLocation.getAddress());
        caqiTextView.setText(myLocation.getCaqi());

        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferences.removeFromArray(myLocation.getLatLng());
                mDataSource.remove(myLocation);
                notifyDataSetChanged();
            }
        });
        return rowView;
    }
}
