package com.pk.informatyka.carbonvalley.notification;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.pk.informatyka.carbonvalley.AirlyApi;
import com.pk.informatyka.carbonvalley.settings.SettingsFragment;

import java.io.IOException;
import java.util.Locale;

public class NotificationService extends Service {

    AirlyApi airlyApi = new AirlyApi(new Activity());

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        stopSelf();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Boolean notifications = sharedPref.getBoolean(SettingsFragment.NOTIFICATIONS, false);
        if (notifications) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            airlyApi.checkIfNotify(latLng, getAddressFromLocation(latLng), getApplicationContext());
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Integer frequency = Integer.valueOf(sharedPref.getString(SettingsFragment.NOTIFICATIONS_FREQUENCY, "10"));
        AlarmManager alarm = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarm.set(
                alarm.RTC_WAKEUP,
                System.currentTimeMillis() + (1000 * 60 * frequency),
                PendingIntent.getService(this, 0, new Intent(this, NotificationService.class), 0)
        );
    }

    @NonNull
    private String getAddressFromLocation(LatLng latLng) {
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            Address address = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1).get(0);
            StringBuilder sb = new StringBuilder();
            sb.append(address.getAddressLine(0));
            if (address.getAddressLine(1) != null) {
                sb.append(", " + address.getAddressLine(1));
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}


