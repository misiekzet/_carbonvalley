package com.pk.informatyka.carbonvalley.mylocations;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class MyLocationsPreferences extends Application {

    private static Context mContext;

    public MyLocationsPreferences(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    public ArrayList<LatLng> getMyLocationsLatLngs() {
        JSONArray jsonArray = loadArray();
        ArrayList<LatLng> latLngs = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                latLngs.add(stringToLatLng(jsonArray.getString(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return latLngs;
    }

    @NonNull
    public LatLng stringToLatLng(String myLocation) {
        String[] latlng =  myLocation.split(",");
        double latitude = Double.parseDouble(latlng[0]);
        double longitude = Double.parseDouble(latlng[1]);
        return new LatLng(latitude, longitude);
    }

    public JSONArray loadArray() {
        SharedPreferences prefs = mContext.getSharedPreferences("myLocationsPref", 0);
        try {
            JSONArray jArray = new JSONArray(prefs.getString("myLocationsArray", ""));
            return jArray;
        } catch (JSONException e) {
            return new JSONArray();
        }
    }

    public void saveArray(JSONArray jArray) {
        SharedPreferences prefs = mContext.getSharedPreferences("myLocationsPref", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("myLocationsArray", jArray.toString());
        editor.commit();
    }

    public void removeFromArray(LatLng latLng) {
        JSONArray myLocationsArray = loadArray();
        myLocationsArray.remove(getMyLocationIndex(latLng));
        saveArray(myLocationsArray);
    }

    public int getMyLocationIndex(LatLng latLng) {
        JSONArray myLocationsArray = loadArray();
        int index = -1;
        for (int i = 0; i < myLocationsArray.length(); i++) {
            try {
                if (myLocationsArray.get(i).toString().equals(latLng.latitude + "," + latLng.longitude)) {
                    index = i;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return index;
    }
}
