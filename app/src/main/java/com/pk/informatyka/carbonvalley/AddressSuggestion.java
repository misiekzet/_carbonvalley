package com.pk.informatyka.carbonvalley;

import android.os.Parcel;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;

public class AddressSuggestion implements SearchSuggestion {

    private String mAddress;

    public AddressSuggestion(String mAddress) {
        this.mAddress = mAddress;
    }

    public AddressSuggestion(Parcel source) {
        this.mAddress = source.readString();
    }

    public static final Creator<AddressSuggestion> CREATOR = new Creator<AddressSuggestion>() {
        @Override
        public AddressSuggestion createFromParcel(Parcel in) {
            return new AddressSuggestion(in);
        }

        @Override
        public AddressSuggestion[] newArray(int size) {
            return new AddressSuggestion[size];
        }
    };

    @Override
    public String getBody() {
        return mAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mAddress);
    }
}
