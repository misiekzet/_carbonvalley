package com.pk.informatyka.carbonvalley.mylocations;

import com.google.android.gms.maps.model.LatLng;

public class MyLocation {

    private String caqi;
    private String address;
    private LatLng latLng;

    public MyLocation(String caqi, String address, LatLng latLng) {
        this.caqi = caqi;
        this.address = address;
        this.latLng = latLng;
    }

    public String getCaqi() {
        return caqi;
    }

    public void setCaqi(String caqi) {
        this.caqi = caqi;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}
