package com.pk.informatyka.carbonvalley;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.pk.informatyka.carbonvalley.indicesdefinition.IndicesDefinitionActivity;
import com.pk.informatyka.carbonvalley.mylocations.MyLocationsActivity;
import com.pk.informatyka.carbonvalley.mylocations.MyLocationsPreferences;
import com.pk.informatyka.carbonvalley.notification.NotificationService;
import com.pk.informatyka.carbonvalley.settings.SettingsActivity;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, OnMapClickListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private AirlyApi airlyApi;
    private FloatingSearchView mSearchView;
    private MyLocationsPreferences preferences;

    private final static LatLng krakow = new LatLng(50.0646501, 19.9449799);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(this, NotificationService.class));
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        PreferenceManager.setDefaultValues(this, R.xml.pref_notification, false);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);
        preferences = new MyLocationsPreferences(this);
        airlyApi = new AirlyApi(this);
        airlyApi.getHeatmapCurrent(mMap);
        setupMap();
        setupSearchBar();
        setupLocationButton();
    }

    @Override
    public void onMapClick(LatLng latLng) {
            airlyApi.getMeasurements(mMap, latLng, getAddressFromLocation(latLng));
    }

    @NonNull
    private String getAddressFromLocation(LatLng latLng) {
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            Address address = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1).get(0);
            StringBuilder sb = new StringBuilder();
            sb.append(address.getAddressLine(0));
            if (address.getAddressLine(1) != null) {
                sb.append(", "+ address.getAddressLine(1));
            }
            return sb.toString();
        }   catch (IOException e) {
                e.printStackTrace();
            return "";
        }
    }

    public void setupMap() {
        mMap.setOnMapClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(krakow, 11));
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(getApplicationContext()));
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                airlyApi.getMeasurements(mMap, latLng, getAddressFromLocation(latLng));
                return false;
            }
        });
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        } else {
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == 0) {
            mMap.setMyLocationEnabled(true);
        }
    }

    public void setupSearchBar() {
        mSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {

            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {

                if (!oldQuery.equals("") && newQuery.equals("")) {
                    mSearchView.clearSuggestions();
                } else {
                    mSearchView.showProgress();
                }
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                List<Address> addressList = new ArrayList<Address>();
                try {
                    addressList = geocoder.getFromLocationName(newQuery, 5);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                List<AddressSuggestion> addressSuggestions = new ArrayList<AddressSuggestion>();
                for (Address address: addressList) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(address.getAddressLine(0));
                    if (address.getAddressLine(1) != null) {
                        sb.append(", "+ address.getAddressLine(1));
                    }
                    addressSuggestions.add(new AddressSuggestion(sb.toString()));
                }
                mSearchView.swapSuggestions(addressSuggestions);
                mSearchView.hideProgress();
            }
        });

        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {

            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                AddressSuggestion addressSuggestion = (AddressSuggestion) searchSuggestion;
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                try {
                    Address address = geocoder.getFromLocationName(addressSuggestion.getBody(), 1).get(0);
                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                    airlyApi.getMeasurements(mMap, latLng, addressSuggestion.getBody());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSearchAction(String currentQuery) {

            }
        });

        mSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_settings:
                        Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.menu_my_locations:
                        Intent intentMyLocations = new Intent(getApplicationContext(), MyLocationsActivity.class);
                        startActivity(intentMyLocations);
                        break;
                    case R.id.menu_indices_definition:
                        Intent intenetIndicesDefinition = new Intent(getApplicationContext(), IndicesDefinitionActivity.class);
                        startActivity(intenetIndicesDefinition);
                        break;
                    case R.id.menu_about:
                        Intent intenetAbout = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(intenetAbout);
                        break;
                }
            }
        });
    }

    private void setupLocationButton() {
        View locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String temp = marker.getPosition().latitude + "," + marker.getPosition().longitude;
        JSONArray jsonArray = preferences.loadArray();
        jsonArray.put(temp);
        preferences.saveArray(jsonArray);
        Toast.makeText(this, "Location added", Toast.LENGTH_LONG).show();
    }
}