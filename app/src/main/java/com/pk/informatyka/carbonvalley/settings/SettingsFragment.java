package com.pk.informatyka.carbonvalley.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.pk.informatyka.carbonvalley.R;

public class SettingsFragment extends PreferenceFragment {

    public static final String NOTIFICATIONS = "notifications_key";
    public static final String CAQI = "min_caqi";
    public static final String NOTIFICATIONS_FREQUENCY = "notifications_frequency";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_notification);
        setHasOptionsMenu(true);
    }
}
