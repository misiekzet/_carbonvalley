package com.pk.informatyka.carbonvalley;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pk.informatyka.carbonvalley.mylocations.MyLocation;
import com.pk.informatyka.carbonvalley.mylocations.MyLocationsAdapter;
import com.pk.informatyka.carbonvalley.notification.NotificationMessage;
import com.pk.informatyka.carbonvalley.settings.SettingsFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AirlyApi {

    private Activity activity;

    private static final String API_URL = "https://airapi.airly.eu/v1/";
    private static final String API_KEY = "e497f53a571747fd8971f287a7695a9d";

    private static final Double SOUTH_WEST_LAT = 49.4;
    private static final Double SOUTH_WEST_LONG = 19.3;
    private static final Double NORTH_EAST_LAT = 50.6;
    private static final Double NORTH_EAST_LONG = 20.6;
    private static final Integer WIDTH_PIXELS = 100;

    private static final String KRAKOW_AREA =
            "southwestLat=" + SOUTH_WEST_LAT +
            "&southwestLong=" + SOUTH_WEST_LONG +
            "&northeastLat=" + NORTH_EAST_LAT +
            "&northeastLong=" + NORTH_EAST_LONG +
            "&widthPixels=" + WIDTH_PIXELS;

    private static final String MEASUREMENTS = "measurements";

    private static final String CURRENT = "current";
    private static final String SENSORS = "sensors/";
    private static final String SENSOR = "sensor/";
    private static final String MAP_POINT = "mapPoint/";
    private static final String HEATMAP = "heatmap/";

    private static final String CELCIUS_DEGREE = "\u00b0C";
    private static final String PERCENT = "%";
    private static final String PRESSURE = "hPa";

    private Marker marker;

    public AirlyApi(Activity activity) {
        this.activity = activity;
    }

    /**
     * Current heatmap for a map region
     */
    public void getHeatmapCurrent(final GoogleMap mMap) {
        RequestQueue queue = Volley.newRequestQueue(activity);
        ImageRequest request = new ImageRequest(API_URL + HEATMAP + CURRENT + "?" + KRAKOW_AREA, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                LatLngBounds latLngBounds = new LatLngBounds(
                        new LatLng(SOUTH_WEST_LAT, SOUTH_WEST_LONG),
                        new LatLng(NORTH_EAST_LAT, NORTH_EAST_LONG));
                GroundOverlayOptions options = new GroundOverlayOptions()
                        .image(BitmapDescriptorFactory.fromBitmap(response))
                        .positionFromBounds(latLngBounds);
                mMap.addGroundOverlay(options);
            }

        }, WIDTH_PIXELS, WIDTH_PIXELS, ImageView.ScaleType.CENTER, Bitmap.Config.RGB_565, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getAuthParams();
            }
        };
        queue.add(request);
    }

    /**
     * Get air quality index and historical data for any point on a map
     * @param sensorId Sensor ID
     */
    public void getMeasurements(Integer sensorId) {
        RequestQueue queue = Volley.newRequestQueue(activity);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                API_URL + SENSOR + MEASUREMENTS + "?sensorId=" + sensorId,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO zrobic metode do oblugi responsa
                System.out.println(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getAuthParams();
            }
        };
        queue.add(request);
    }

    /**
     * Sensor's detailed measurements and historical data
     * @param mMap Activity GoogleMap
     * @param latLng Latitude and longitude coordinate
     * @param address Address
     */
    public void getMeasurements(final GoogleMap mMap, final LatLng latLng, final String address) {
        addMeasurementMarker(mMap, latLng, address, "Loading...");
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, mMap.getCameraPosition().zoom));
        RequestQueue queue = Volley.newRequestQueue(activity);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                API_URL + MAP_POINT + MEASUREMENTS + "?latitude=" + latLng.latitude + "&longitude=" + latLng.longitude,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    addMeasurementMarker(mMap, latLng, address, getAirQualityInfo(response));
                } catch (JSONException e) {
                    addMeasurementMarker(mMap, latLng, address, "Location not available yet");
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getAuthParams();
            }
        };
        queue.add(request);
    }

    public void checkIfNotify(final LatLng latLng, final String address, final Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                API_URL + MAP_POINT + MEASUREMENTS + "?latitude=" + latLng.latitude + "&longitude=" + latLng.longitude,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Integer caqi = getCaqiFromResponse(response);
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
                Integer minCaqi = Integer.valueOf(sharedPref.getString(SettingsFragment.CAQI, "60"));
                if (caqi != null && caqi >= minCaqi) {
                    NotificationMessage.notify(context, caqi.toString(), address, 0);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getAuthParams();
            }
        };
        queue.add(request);
    }

    private void addMeasurementMarker(GoogleMap mMap, LatLng latLng, String title, String snippet) {
        removeMarker();
        marker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        marker.showInfoWindow();
    }

    /**
     * Current sensors list for a map region
     */
    public void getSensorsCurrent() {
        RequestQueue queue = Volley.newRequestQueue(activity);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                API_URL + SENSORS + CURRENT + "?" + KRAKOW_AREA,
                null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //TODO zrobic metode do oblugi responsa
                System.out.println(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getAuthParams();
            }
        };
        queue.add(request);
    }

    /**
     * Sensor's info
     * @param sensorId Sensor ID
     */
    public void getSensor(Integer sensorId) {
        RequestQueue queue = Volley.newRequestQueue(activity);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                API_URL + SENSORS + "/" + sensorId,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO zrobic metode do oblugi responsa
                System.out.println(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getAuthParams();
            }
        };
        queue.add(request);
    }

    public void getMeasurements(final MyLocationsAdapter adapter, final ArrayList<MyLocation> myLocationListView, ArrayList<LatLng> myLocationDtoList) {
        RequestQueue queue = Volley.newRequestQueue(activity);
        for (final LatLng latLng: myLocationDtoList) {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                    API_URL + MAP_POINT + MEASUREMENTS + "?latitude=" + latLng.latitude + "&longitude=" + latLng.longitude,
                    null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Integer caqi = getCaqiFromResponse(response);
                    myLocationListView.add(new MyLocation((!caqi.toString().equals("-1")) ? caqi.toString() : "    ", getAddressFromLocation(latLng), latLng));
                    adapter.notifyDataSetChanged();
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return getAuthParams();
                }
            };
            queue.add(request);
        }
    }

    @NonNull
    private Map<String, String> getAuthParams() {
        Map<String, String> params = new HashMap<>();
        params.put("apikey", API_KEY);
        return params;
    }

    private String getAirQualityInfo(JSONObject response) throws JSONException {
        StringBuilder info = new StringBuilder();
        info.append("CAQI: " + getRoundedValueFromResponse(response, "airQualityIndex") + System.lineSeparator());
        info.append("PM 2.5: " + getRoundedValueFromResponse(response, "pm25") + System.lineSeparator());
        info.append("PM 10: " + getRoundedValueFromResponse(response, "pm10") + System.lineSeparator());
        try {
            info.append("Temperature: " + getRoundedValueFromResponse(response, "temperature") + CELCIUS_DEGREE + System.lineSeparator());
        } catch (JSONException e) { }
        try {
            info.append("Humidity: " + getRoundedValueFromResponse(response, "humidity") + PERCENT + System.lineSeparator());
        } catch (JSONException e) { }
        info.append("Pressure: " + getRoundedDividedValueFromResponse(response, "pressure", 100) + PRESSURE);
        return info.toString();
    }

    private Integer getCaqiFromResponse(JSONObject response) {
        try {
            return Integer.valueOf(getRoundedValueFromResponse(response, "airQualityIndex"));
        } catch (JSONException e) {
            return -1;
        }
    }

    private String getRoundedValueFromResponse(JSONObject response, String value) throws JSONException {
        return getRoundedString(response.getJSONObject("currentMeasurements").getString(value));
    }

    private String getRoundedDividedValueFromResponse(JSONObject response, String value, int divider) throws JSONException {
        return getDividedRoundedString(response.getJSONObject("currentMeasurements").getString(value), divider);
    }

    private String getRoundedString(String string) {
        DecimalFormat df = new DecimalFormat("#");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(new Double(string));
    }

    private String getDividedRoundedString(String string, int divider) {
        DecimalFormat df = new DecimalFormat("#");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(new Double(string) / divider);
    }

    private void removeMarker() {
        if (marker != null) {
            marker.remove();
        }
    }

    @NonNull
    private String getAddressFromLocation(LatLng latLng) {
        try {
            Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
            Address address = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1).get(0);
            StringBuilder sb = new StringBuilder();
            sb.append(address.getAddressLine(0));
            if (address.getAddressLine(1) != null) {
                sb.append(", "+ address.getAddressLine(1));
            }
            return sb.toString();
        }   catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}