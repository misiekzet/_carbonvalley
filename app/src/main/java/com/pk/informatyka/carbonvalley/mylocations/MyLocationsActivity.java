package com.pk.informatyka.carbonvalley.mylocations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.pk.informatyka.carbonvalley.AirlyApi;
import com.pk.informatyka.carbonvalley.R;

import java.util.ArrayList;

public class MyLocationsActivity extends AppCompatActivity {

    private ListView mListView;
    private AirlyApi airlyApi;
    private MyLocationsPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_locations);
        mListView = (ListView) findViewById(R.id.my_locations_list_view);
        airlyApi = new AirlyApi(this);
        ArrayList<MyLocation> myLocationListView = new ArrayList<>();
        MyLocationsAdapter adapter = new MyLocationsAdapter(this, myLocationListView);
        mListView.setAdapter(adapter);
        preferences = new MyLocationsPreferences(this);
        airlyApi.getMeasurements(adapter, myLocationListView, preferences.getMyLocationsLatLngs());
    }
}