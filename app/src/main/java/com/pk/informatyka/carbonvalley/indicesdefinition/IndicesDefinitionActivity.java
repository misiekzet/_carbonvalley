package com.pk.informatyka.carbonvalley.indicesdefinition;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.pk.informatyka.carbonvalley.R;

public class IndicesDefinitionActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.indices_definition);
    }
}
