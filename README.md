## Opis projektu

Carbon Valley jest aplikacją mobilną na urządzenia z systemem Android od wersji 5.0 (Lollipop - SDK 21). Aplikacja służy do monitorowania stanu zanieczyszczenia powietrza w Krakowie. 

Carbon Valley korzysta z udostępnionego przez projekt Airly API. Dzięki API, aplikacja jest w stanie pobierać i wyświetlać dokładne pomiary:

* CAQI,
* PM2.5,
* PM10,
* temperatury,
* ciśnienia,

dla wybranej lokalizacji w prostej i przejrzystej formie.

## Struktura katalogów

W tej sekcji zostaną opisane najważniejsze pliki/katalogi w projekcie

* /app - pliki konfiguracyjne
    * /src
    * `build.grandle`- skrypt zarządzający system budowania aplikacji
        * /main 
            * /res - layouty, grafiki, stringi, wymiary
            * `AndroidManifest.xml` - najważniejsze informacje o aplikacji wymagane przez system Android
            * /java/com/pk/informatyka/carbonvalley
                * indicesdefinition - katalog z klasą zarządzają zakładką w menu - Indices definition
                * mylocations - katalog z klasami zarządzającymi zakładką w menu - My locations
                * notification - katalog z klasami obsługującymi notyfikacje
                * settings - katalog z klasami zarządzającymi zakładką w menu - Settings
                * `AboutActivity.java` - katalog z klasami zarządzającymi zakładką w menu - About
                * `AirlyApi.java` - klasa oparta o wzorzec fasada - służy do wysyłania reqestów i obsługi responsów AirlyAPI
                * `MapsActivity.java` - główna aktywność aplikacji

## Dalszy rozwój aplikacji

W celu rozwijania aplikacji we własnym zakresie opierając się o zaimplementowany wzorzec należy, w pierwszej kolejności zagłębić się w klasę `AirlyAPI` znajdującą się w katalogu: 
`/app/src/main/java/com/pk/informatyka/carbonvalley/`.

Metody we wspomnianej klasie są odwzorowaniem możliwości samego API http://apidocs.airly.eu/.

Każda funkcja służy do obsługi poszczególnych requestów/responsów. W celu rozszerzenia funkcjonalności aplikacji opartych o API należy w pierwszej kolejności zmodyfikować odpowiednią metodę w wspomnianej wcześniej klasie.

Strona API: https://airly.eu/en/api/